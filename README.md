# ApiHelper


ApiHelper is a library that implements the basic elements for synchronous and asynchronous http requests and implements the objects for Response, Request and Parameters management of a call.

<div align="center">
	<br>
	<br>
	<p>
		ApiHelper is an open source project support by the community of 'PaironsTech Open Projects'
		<br>
		<br>
		<a href="https://www.patreon.com/PaironsTech">
			<img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
		</a>
	</p>
	<br>
	<br>
</div>


## Motivation

We needed a library that contained "standard" methods and managements, so that we could create libraries for API services faster.


## Frameworks Supported

- NetCoreApp 2.2


## Dependencies

- Newtonsoft.Json - version 12.0.1 


## Features

- Constantly updated.
- Less code and more speed in creating libraries for API.
- Automatic Serialization / Deserialization from / to objects based on content/type of the call.
- Interfaces for management response, request and params object with validation of data inserted.
- Caching of object properties for better performance.
- Error handling thanks to the object 'ResponseGeneric'.

## Content/Type Supported

- Application/Json


## Code Examples

### Example of Response Object

```csharp
using Newtonsoft.Json;
using PaironsTech.ApiHelper.Interfaces;

namespace ApiHelper.Examples
{

    /// <summary>
    /// Example of Response Object
    /// </summary>
    public class ResponseExample : IResponse
    {

        /// <summary>
        /// A Boolean that rappresent if a data changed where.
        /// The true name of param returned is 'data_changed'
        /// </summary>
        [JsonProperty("data_changed")]
        public bool DataChanged { get; set; }

        /// <summary>
        /// A DateTime that rappresent the date and the time of the last edit. 
        /// The truename of param returned is 'datetime_last_edit' 
        ///     and the value returned is in milliseconds. 
        /// We used JsonConverterDateTimeMilliseconds Converter.
        /// </summary>
        [JsonProperty("datetime_last_edit")]
        [JsonConverter(typeof(JsonConverterDateTimeMilliseconds))]
        public DateTime DateTimeLastEdit { get; set; }

    }

}
```

### Example of Request Object

```csharp
using Newtonsoft.Json;
using PaironsTech.ApiHelper.Interfaces;

namespace ApiHelper.Examples
{

    /// <summary>
    /// Example of Request Object
    /// </summary>
    public class RequestExample : IRequest
    {

        /// <summary>
        /// A string that rappresent the new Name of the Data
        /// </summary>
        [JsonProperty("data_name")]
        public string DataName { get; set; }

        /// <summary>
        /// Validation method of data [From Interface 'IRequest']
        /// </summary>
        public void ValidateData()
        {
            if (string.IsNullOrEmpty(DataName))
            {
                throw new ArgumentNullException("DataName");
            }
        }

    }

}
```

### Example of Params Object

```csharp
using PaironsTech.ApiHelper.Attributes;
using PaironsTech.ApiHelper.Interfaces;

namespace ApiHelper.Examples
{

    /// <summary>
    /// Example of Params Object
    /// </summary>
    public class ParamsExample : IParams
    {

        /// <summary>
        /// A string that rappresent the Id of the Data
        /// </summary>
        [ParamProperty("data_id")]
        public string IdData { get; set; }

        /// <summary>
        /// The constructor of ParamsExample
        /// </summary>
        /// <param name="dataId">the id of the data to edit</param>
        public ParamsAccessToken(string dataId)
        {
            IdData = dataId;
        }

        /// <summary>
        /// Validation method of data [From Interface 'IParams']
        /// </summary>
        public void ValidateData()
        {
            if (string.IsNullOrEmpty(IdData))
            {
                throw new ArgumentNullException("IdData");
            }
        }

    }

}
```

### Example of API Class and Method


```csharp
using PaironsTech.ApiHelper;
using System;

namespace ApiHelper.Examples
{
    /// <summary>
    /// Example Object that interact through methods the API 
    /// </summary>
    public class ExampleAPI
    {

        /// <summary>
        /// Base Address of API call
        /// </summary>
        private static readonly Uri _baseAddress = new Uri("https://example-api.com/api/");

        /// <summary>
        /// The Access Token to add during the request
        /// </summary>
        private static readonly string _accessToken = "XXXXXXXXXXXXXXXXXXXXXX";

        /// <summary>
        /// Edit the Data
        /// </summary>
        /// <returns>ResponseGeneric with ResponseExample response object</returns>
        public ResponseGeneric<ResponseExample, ResponseExampleError> EditExample(
            ParamsExample paramsExample, RequestExample requestExample)
        {
            // Address Uri
            // '{data_id}' will be substitute by the value of ParamExample
            Uri addressUri = new Uri("edit/{data_id}", UriKind.Relative);

            // Headers
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("access_token", _accessToken);

            // Execute Call
            return HttpRequest.ExecutePutCall<ResponseExample, ResponseExampleError>
            (
                _baseAddress, 
                addressUri, 
                paramsData: paramsExample, 
                requestData: requestExample, 
                httpRequestOptions: new HttpRequestSettings()
                {
                    Headers = headers,
                    ContentTypeRequest = "application/json"
                }
            );
        }
    }
}

```

## Installation

You can download the ApiHelper library from [NuGet](https://www.nuget.org/packages/Api-Helper).


## Technical Documentation / How to Use

if you want to know how the library works in deep or if you want to know how to use the library you can find our technical documentation at the following [link](https://www.paironstech.com/open-projects/api-helper/docs).


## Contribute & Support

If you have ideas to improve the software or want to report bugs you can open an issue.

While if you want to participate in the creation of open source projects you can contact us at the email openprojects@paironstech.com

If you want to support the 'ApiHelper' project you can subscribe to our [Patreon](https://www.patreon.com/PaironsTech). 

<div align="center">
	<br>
	<br>
	<p>
		<br>
		<br>
		<a href="https://www.patreon.com/PaironsTech">
			<img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
		</a>
	</p>
	<br>
	<br>
</div>



## Contributors

- Fabrizio Pairone ([GitHub](https://github.com/FabrizioPairone) - [GitLab](https://gitlab.com/FabrizioPairone))


## Projects using ApiHelper

- [ClickUpAPI](https://github.com/PaironsTech/ClickUpAPI) ([GitLab](https://gitlab.com/PaironsTech/ClickUpAPI) / [GitHub](https://github.com/PaironsTech/ClickUpAPI))

## License

Apache version 2.0. (Show the file LICENSE.md)