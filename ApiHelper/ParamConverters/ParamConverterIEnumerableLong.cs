﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PaironsTech.ApiHelper.ParamConverters
{

    /// <summary>
    /// Param Converter class of IEnumerable long object
    /// </summary>
    public class ParamConverterIEnumerableLong : ParamConverter
    {

        /// <summary>
        /// Check if can convert this type of object. (Can only convert IEnumerable long)
        /// </summary>
        /// <param name="objectType">object Type</param>
        /// <returns>Bool if can convert this object</returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(IEnumerable<long>).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Get Ienumerable long and serialize it
        /// </summary>
        /// <param name="value">the original object value</param>
        /// <returns>String that contains the value serialized</returns>
        public override string SerializeParamValue(object value)
        {
            return string.Join("&{NAME_PARAM}=", ((IEnumerable<long>)value).Select(s => s.ToString()));
        }

    }

}
