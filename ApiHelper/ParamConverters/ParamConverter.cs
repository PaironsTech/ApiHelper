﻿using System;

namespace PaironsTech.ApiHelper.ParamConverters
{

    /// <summary>
    /// Abstract class for ParamConverters
    /// </summary>
    public abstract class ParamConverter
    {

        /// <summary>
        /// Check if the type can be converted by the converter
        /// </summary>
        /// <param name="type">The type of the object to convert</param>
        /// <returns>Boolean that rappresent if the value can be converted by converter</returns>
        public abstract bool CanConvert(Type type);

        /// <summary>
        /// Serialize the value
        /// </summary>
        /// <param name="value">object value to serialize</param>
        /// <returns>The object serialized</returns>
        public abstract string SerializeParamValue(object value);
       
    }

}
