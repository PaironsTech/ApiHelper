﻿using System;

namespace PaironsTech.ApiHelper.ParamConverters
{

    /// <summary>
    /// Param Converter class of TimeSpan object to / from milliseconds time
    /// </summary>
    public class ParamConverterTimeSpanMilliseconds : ParamConverter
    {

        /// <summary>
        /// Check if can convert this type of object. (Can only convert TimeSpan)
        /// </summary>
        /// <param name="objectType">object Type</param>
        /// <returns>Bool if can convert this object</returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(TimeSpan).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Get TimeSpan and serialize it to long for Unix method
        /// </summary>
        /// <param name="value">the original object value</param>
        /// <returns>String that contains the value serialized</returns>
        public override string SerializeParamValue(object value)
        {
            return ((TimeSpan)value).TotalMilliseconds.ToString();
        }

    }

}
