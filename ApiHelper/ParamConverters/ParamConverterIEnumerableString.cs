﻿using System;
using System.Collections.Generic;

namespace PaironsTech.ApiHelper.ParamConverters
{

    /// <summary>
    /// Param Converter class of IEnumerable string object
    /// </summary>
    public class ParamConverterIEnumerableString : ParamConverter
    {

        /// <summary>
        /// Check if can convert this type of object. (Can only convert IEnumerable string)
        /// </summary>
        /// <param name="objectType">object Type</param>
        /// <returns>Bool if can convert this object</returns>
        public override bool CanConvert(Type objectType)
        {
            return typeof(IEnumerable<string>).IsAssignableFrom(objectType);
        }

        /// <summary>
        /// Get Ienumerable string and serialize it
        /// </summary>
        /// <param name="value">the original object value</param>
        /// <returns>String that contains the value serialized</returns>
        public override string SerializeParamValue(object value)
        {
            return string.Join("&{NAME_PARAM}=", (IEnumerable<string>)value);
        }

    }

}
