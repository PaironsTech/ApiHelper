﻿using PaironsTech.ApiHelper.ParamConverters;
using System;

namespace PaironsTech.ApiHelper.Attributes
{

    /// <summary>
    /// ParamConverter Attribute, for set the converter of the property
    /// </summary>
    public class ParamConverterAttribute : Attribute
    {

        /// <summary>
        /// Type of the converter used for the property that have the attribute
        /// </summary>
        public Type ConverterType { get; }





        /// <summary>
        /// Constructor of the ParamConverter Attribute
        /// </summary>
        /// <param name="converterType">The type of the converter</param>
        public ParamConverterAttribute(Type converterType)
        {
            if (typeof(ParamConverter).IsAssignableFrom(converterType))
            {
                ConverterType = converterType;
            }
            else
            {
                throw new Exception("The converterType passed as param must be a sub class of abstract class 'ParamConverter'. '" + converterType.Name + "' is not a subclass of 'ParamConverter'.");
            }
        }
        
    }

}
