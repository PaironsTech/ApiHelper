﻿using System;

namespace PaironsTech.ApiHelper.Attributes
{

    /// <summary>
    /// ParamProperty Attribute, for set the settings of the param
    /// </summary>
    public class ParamPropertyAttribute : Attribute
    {

        /// <summary>
        /// The True name of the Param
        /// </summary>
        public string NameParam { get; }





        /// <summary>
        /// Constructor of the Param Property Attribute
        /// </summary>
        /// <param name="nameParam"></param>
        public ParamPropertyAttribute(string nameParam)
        {
            NameParam = nameParam;
        }

    }

}
