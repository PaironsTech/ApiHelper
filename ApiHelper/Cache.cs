﻿using Newtonsoft.Json;
using PaironsTech.ApiHelper.Attributes;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PaironsTech.ApiHelper
{

    /// <summary>
    /// Static class used for caching data
    /// </summary>
    internal static class Cache
    {

        #region Caching

        /// <summary>
        /// Caching the properties of types
        /// </summary>
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, List<PropertyInfoSupport>> PropertiesType = new ConcurrentDictionary<RuntimeTypeHandle, List<PropertyInfoSupport>>();

        #endregion


        #region Useful Class for Caching

        /// <summary>
        /// Support class for caching the property Info
        /// </summary>
        internal class PropertyInfoSupport
        {

            /// <summary>
            /// Json Property Attribute Associated at the PropertyInfo
            /// </summary>
            public JsonPropertyAttribute JsonPropertyAssociated { get; set; }

            /// <summary>
            /// Json Converter Attribute Associated at the PropertyInfo
            /// </summary>
            public JsonConverterAttribute JsonConverterAssociated { get; set; }

            /// <summary>
            /// Paramm Converter Attribute Associated at the Property Info
            /// </summary>
            public ParamConverterAttribute ParamConverterAssociated { get; set; }

            /// <summary>
            /// Param Properrty Attribute Associated at the Property Info
            /// </summary>
            public ParamPropertyAttribute ParamPropertyAssociated { get; set; }
            
            /// <summary>
            /// Property Info Object
            /// </summary>
            public PropertyInfo PropertyInfo { get; set; }





            /// <summary>
            /// Constructor of PropertyInfoSupport object
            /// </summary>
            /// <param name="propertyInfo">The Property Info object</param>
            public PropertyInfoSupport(PropertyInfo propertyInfo)
            {
                PropertyInfo = propertyInfo;

                if (PropertyInfo != null)
                {
                    List<Attribute> attributes = PropertyInfo.GetCustomAttributes().ToList();

                    // Find JsonPropertyAttribute
                    JsonPropertyAssociated = (JsonPropertyAttribute)attributes.Find(f => f.GetType() == typeof(JsonPropertyAttribute));

                    // Find JsonConverterAttribute
                    JsonConverterAssociated = (JsonConverterAttribute)attributes.Find(f => f.GetType() == typeof(JsonConverterAttribute));

                    // Find ParamConverterAttribute
                    ParamConverterAssociated = (ParamConverterAttribute)attributes.Find(f => f.GetType() == typeof(ParamConverterAttribute));

                    // Find ParamPropertyAttribute
                    ParamPropertyAssociated = (ParamPropertyAttribute)attributes.Find(f => f.GetType() == typeof(ParamPropertyAttribute));

                }

            }

        }

        #endregion


        #region Method Caching

        /// <summary>
        /// Get the list of PropertyInfo cached for this Type
        /// </summary>
        /// <param name="type">Type of the object </param>
        /// <returns>List of property info of the object</returns>
        internal static List<PropertyInfoSupport> GetPropertiesCached(this Type type)
        {
            if (PropertiesType.TryGetValue(type.TypeHandle, out List<PropertyInfoSupport> propertiesCached)) return propertiesCached;
            List<PropertyInfoSupport> properties = type.GetProperties().Select(propertyInfo => new PropertyInfoSupport(propertyInfo)).ToList();
            PropertiesType[type.TypeHandle] = properties;
            return properties;
        }

        #endregion

    }

}
