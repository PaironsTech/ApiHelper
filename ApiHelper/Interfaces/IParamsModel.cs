﻿namespace PaironsTech.ApiHelper.Interfaces
{

    /// <summary>
    /// The interface of all "Params/Model" objects
    /// </summary>
    public interface IParamsModel : IParams
    {

        /// <summary>
        /// This method check the validation of data inserted for the request. Check obbligatoriness, data type, etc.
        /// </summary>
        new void ValidateData();

    }

}
