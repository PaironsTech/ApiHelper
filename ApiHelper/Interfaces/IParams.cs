﻿namespace PaironsTech.ApiHelper.Interfaces
{

    /// <summary>
    /// The interface of all "Params" objects
    /// </summary>
    public interface IParams
    {

        /// <summary>
        /// This method check the validation of data inserted for the request. Check obbligatoriness, data type, etc.
        /// </summary>
        void ValidateData();

    }

}
