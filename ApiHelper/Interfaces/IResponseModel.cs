﻿namespace PaironsTech.ApiHelper.Interfaces
{

    /// <summary>
    /// The interface of all "Response/Model" objects
    /// </summary>
    public interface IResponseModel : IResponse { }

}
