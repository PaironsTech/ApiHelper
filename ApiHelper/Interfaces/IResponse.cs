﻿namespace PaironsTech.ApiHelper.Interfaces
{

    /// <summary>
    /// The Interface of all "Response" objects
    /// </summary>
    public interface IResponse { }

}
