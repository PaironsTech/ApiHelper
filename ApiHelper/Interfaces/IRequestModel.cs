﻿namespace PaironsTech.ApiHelper.Interfaces
{

    /// <summary>
    /// The interface of all "Request/Model" objects
    /// </summary>
    public interface IRequestModel : IRequest
    {

        /// <summary>
        /// This method check the validation of data inserted for the request. Check obbligatoriness, data type, etc.
        /// </summary>
        new void ValidateData();

    }
}
