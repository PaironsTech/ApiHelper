﻿using Newtonsoft.Json;
using PaironsTech.ApiHelper.Interfaces;
using PaironsTech.ApiHelper.ParamConverters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace PaironsTech.ApiHelper
{

    /// <summary>
    /// Static class that contains useful methods for Http Request and managed Response
    /// </summary>
    public static class HttpRequest
    {

        #region Sync Request


        /// <summary>
        /// Method that execute synchronous http GET request
        /// </summary>
        /// <typeparam name="TResponseSuccessExpected">Type of Response if call return status equals to 200</typeparam>
        /// <typeparam name="TResponseErrorExpected">Type of Response if call not return status equals to 200</typeparam>
        /// <param name="baseAddress">Base address of request</param>
        /// <param name="requestUri">The request uri of the call</param>
        /// <param name="paramsData">The object that contains the params of the call</param>
        /// <param name="httpRequestOptions">The option of the request</param>
        /// <returns>ResponseGeneric Object with the response of the request.</returns>
        public static ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> ExecuteGetCall<TResponseSuccessExpected, TResponseErrorExpected>(Uri baseAddress,
            Uri requestUri, IParams paramsData = null, HttpRequestSettings httpRequestOptions = null)
            where TResponseSuccessExpected : IResponse where TResponseErrorExpected : IResponse
        {
            ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> responseGeneric = new ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>();

            Task.Run(async () =>
            {
                responseGeneric = await ExecuteGetCallAsync<TResponseSuccessExpected, TResponseErrorExpected>(baseAddress, requestUri, paramsData, httpRequestOptions);
            })
            .Wait();

            return responseGeneric;
        }

        /// <summary>
        /// Method that execute asynchronous http POST request
        /// </summary>
        /// <typeparam name="TResponseSuccessExpected">Type of Response if call return status equals to 200</typeparam>
        /// <typeparam name="TResponseErrorExpected">Type of Response if call not return status equals to 200</typeparam>
        /// <param name="baseAddress">Base address of request</param>
        /// <param name="requestUri">The request uri of the call</param>
        /// <param name="paramsData">The object that contains the params of the call</param>
        /// <param name="requestData">The object that rappresent the request content of the call</param>
        /// <param name="httpRequestOptions">The option of the request</param>
        /// <param name="httpRequestOptions"></param>
        /// <returns>ResponseGeneric Object with the response of the request.</returns>
        public static ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> ExecutePostCall<TResponseSuccessExpected, TResponseErrorExpected>(Uri baseAddress,
            Uri requestUri, IParams paramsData = null, IRequest requestData = null, HttpRequestSettings httpRequestOptions = null)
            where TResponseSuccessExpected : IResponse where TResponseErrorExpected : IResponse
        {
            ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> responseGeneric = new ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>();

            Task.Run(async () =>
            {
                responseGeneric = await ExecutePostCallAsync<TResponseSuccessExpected, TResponseErrorExpected>(baseAddress, requestUri, paramsData, requestData, httpRequestOptions);
            })
            .Wait();

            return responseGeneric;
        }

        /// <summary>
        /// Method that execute asynchronous http PUT request
        /// </summary>
        /// <typeparam name="TResponseSuccessExpected">Type of Response if call return status equals to 200</typeparam>
        /// <typeparam name="TResponseErrorExpected">Type of Response if call not return status equals to 200</typeparam>
        /// <param name="baseAddress">Base address of request</param>
        /// <param name="requestUri">The request uri of the call</param>
        /// <param name="paramsData">The object that contains the params of the call</param>
        /// <param name="requestData">The object that rappresent the request content of the call</param>
        /// <param name="httpRequestOptions">The option of the request</param>
        /// <param name="httpRequestOptions"></param>
        /// <returns>ResponseGeneric Object with the response of the request.</returns>
        public static ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> ExecutePutCall<TResponseSuccessExpected, TResponseErrorExpected>(Uri baseAddress,
            Uri requestUri, IParams paramsData = null, IRequest requestData = null, HttpRequestSettings httpRequestOptions = null)
            where TResponseSuccessExpected : IResponse where TResponseErrorExpected : IResponse
        {
            ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> responseGeneric = new ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>();

            Task.Run(async () =>
            {
                responseGeneric = await ExecutePutCallAsync<TResponseSuccessExpected, TResponseErrorExpected>(baseAddress, requestUri, paramsData, requestData, httpRequestOptions);
            })
            .Wait();

            return responseGeneric;
        }

        /// <summary>
        /// Method that execute synchronous http DELETE request
        /// </summary>
        /// <typeparam name="TResponseSuccessExpected">Type of Response if call return status equals to 200</typeparam>
        /// <typeparam name="TResponseErrorExpected">Type of Response if call not return status equals to 200</typeparam>
        /// <param name="baseAddress">Base address of request</param>
        /// <param name="requestUri">The request uri of the call</param>
        /// <param name="paramsData">The object that contains the params of the call</param>
        /// <param name="httpRequestOptions">The option of the request</param>
        /// <returns>ResponseGeneric Object with the response of the request.</returns>
        public static ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> ExecuteDeleteCall<TResponseSuccessExpected, TResponseErrorExpected>(Uri baseAddress,
            Uri requestUri, IParams paramsData = null, HttpRequestSettings httpRequestOptions = null)
            where TResponseSuccessExpected : IResponse where TResponseErrorExpected : IResponse
        {
            ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> responseGeneric = new ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>();

            Task.Run(async () =>
            {
                responseGeneric = await ExecuteDeleteCallAsync<TResponseSuccessExpected, TResponseErrorExpected>(baseAddress, requestUri, paramsData, httpRequestOptions);
            })
            .Wait();

            return responseGeneric;
        }

        #endregion


        #region Async Request

        /// <summary>
        /// Method that execute asynchronous http GET request
        /// </summary>
        /// <typeparam name="TResponseSuccessExpected">Type of Response if call return status equals to 200</typeparam>
        /// <typeparam name="TResponseErrorExpected">Type of Response if call not return status equals to 200</typeparam>
        /// <param name="baseAddress">Base address of request</param>
        /// <param name="requestUri">The request uri of the call</param>
        /// <param name="paramsData">The object that contains the params of the call</param>
        /// <param name="httpRequestOptions">The option of the request</param>
        /// <returns>Task of ResponseGeneric Object with the response of the request.</returns>
        public static async Task<ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>> ExecuteGetCallAsync<TResponseSuccessExpected, TResponseErrorExpected>(Uri baseAddress, 
            Uri requestUri, IParams paramsData = null, HttpRequestSettings httpRequestOptions = null) 
            where TResponseSuccessExpected : IResponse where TResponseErrorExpected : IResponse
        {
            // Execute Controls
            if (paramsData != null) paramsData.ValidateData();
            if (httpRequestOptions == null) httpRequestOptions = new HttpRequestSettings();
            requestUri = ResolveUriWithParams(requestUri, paramsData);

            ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> responseGeneric = new ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>();

            using (HttpClient httpClient = new HttpClient { BaseAddress = baseAddress })
            {

                // Adding Headers if they are setted
                if (httpRequestOptions.Headers != null)
                {
                    foreach (KeyValuePair<string, string> header in httpRequestOptions.Headers)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                    }
                }
                
                // Execute GET Async Method
                using (var httpResponse = await httpClient.GetAsync(requestUri))
                {
                    string responseData = await httpResponse.Content.ReadAsStringAsync();
                    responseGeneric.RequestStatus = httpResponse.StatusCode;

                    switch (httpResponse.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            responseGeneric.ResponseSuccess = DeserializeResponse<TResponseSuccessExpected>(httpResponse.Content.Headers.ContentType.MediaType, responseData);
                            break;
                        default:
                            responseGeneric.ResponseError = DeserializeResponse<TResponseErrorExpected>(httpResponse.Content.Headers.ContentType.MediaType, responseData);
                            break;
                    }
                }
            }

            return responseGeneric;
        }

        /// <summary>
        /// Method that execute asynchronous http POST request
        /// </summary>
        /// <typeparam name="TResponseSuccessExpected">Type of Response if call return status equals to 200</typeparam>
        /// <typeparam name="TResponseErrorExpected">Type of Response if call not return status equals to 200</typeparam>
        /// <param name="baseAddress">Base address of request</param>
        /// <param name="requestUri">The request uri of the call</param>
        /// <param name="paramsData">The object that contains the params of the call</param>
        /// <param name="requestData">The object that rappresent the request content of the call</param>
        /// <param name="httpRequestOptions">The option of the request</param>
        /// <returns>Task of ResponseGeneric Object with the response of the request</returns>
        public static async Task<ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>> ExecutePostCallAsync<TResponseSuccessExpected, TResponseErrorExpected>(Uri baseAddress,
            Uri requestUri, IParams paramsData = null, IRequest requestData = null, HttpRequestSettings httpRequestOptions = null)
            where TResponseSuccessExpected : IResponse where TResponseErrorExpected : IResponse
        {
            // Execute Controls
            if (paramsData != null) paramsData.ValidateData();
            if (requestData != null) requestData.ValidateData();
            if (httpRequestOptions == null) httpRequestOptions = new HttpRequestSettings();
            requestUri = ResolveUriWithParams(requestUri, paramsData);

            ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> responseGeneric = new ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>();

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {

                // Adding Headers if they are setted
                if (httpRequestOptions.Headers != null)
                {
                    foreach (KeyValuePair<string, string> header in httpRequestOptions.Headers)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                    }
                }

                // Adding Request Content
                using (var content = SerializeRequest(httpRequestOptions.ContentTypeRequest, requestData))
                {
                    // Execute POST Async Method
                    using (var httpResponse = await httpClient.PostAsync(requestUri, content))
                    {
                        string responseData = await httpResponse.Content.ReadAsStringAsync();
                        responseGeneric.RequestStatus = httpResponse.StatusCode;

                        switch (httpResponse.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                responseGeneric.ResponseSuccess = DeserializeResponse<TResponseSuccessExpected>(httpResponse.Content.Headers.ContentType.MediaType, responseData);
                                break;
                            default:
                                responseGeneric.ResponseError = DeserializeResponse<TResponseErrorExpected>(httpResponse.Content.Headers.ContentType.MediaType, responseData);
                                break;
                        }
                    }
                }
            }

            return responseGeneric;
        }

        /// <summary>
        /// Method that execute asynchronous http PUT request
        /// </summary>
        /// <typeparam name="TResponseSuccessExpected">Type of Response if call return status equals to 200</typeparam>
        /// <typeparam name="TResponseErrorExpected">Type of Response if call not return status equals to 200</typeparam>
        /// <param name="baseAddress">Base address of request</param>
        /// <param name="requestUri">The request uri of the call</param>
        /// <param name="paramsData">The object that contains the params of the call</param>
        /// <param name="requestData">The object that rappresent the request content of the call</param>
        /// <param name="httpRequestOptions">The option of the request</param>
        /// <returns>Task of ResponseGeneric Object with the response of the request</returns>
        public static async Task<ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>> ExecutePutCallAsync<TResponseSuccessExpected, TResponseErrorExpected>(Uri baseAddress,
            Uri requestUri, IParams paramsData = null, IRequest requestData = null, HttpRequestSettings httpRequestOptions = null)
            where TResponseSuccessExpected : IResponse where TResponseErrorExpected : IResponse
        {
            // Execute Controls
            if (paramsData != null) paramsData.ValidateData();
            if (requestData != null) requestData.ValidateData();
            if (httpRequestOptions == null) httpRequestOptions = new HttpRequestSettings();
            requestUri = ResolveUriWithParams(requestUri, paramsData);

            ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> responseGeneric = new ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>();

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {

                // Adding Headers if they are setted
                if (httpRequestOptions.Headers != null)
                {
                    foreach (KeyValuePair<string, string> header in httpRequestOptions.Headers)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                    }
                }

                // Adding Request Content
                using (var content = SerializeRequest(httpRequestOptions.ContentTypeRequest, requestData))
                {
                    // Execute PUT Async Method
                    using (var httpResponse = await httpClient.PutAsync(requestUri, content))
                    {
                        string responseData = await httpResponse.Content.ReadAsStringAsync();
                        responseGeneric.RequestStatus = httpResponse.StatusCode;

                        switch (httpResponse.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                responseGeneric.ResponseSuccess = DeserializeResponse<TResponseSuccessExpected>(httpResponse.Content.Headers.ContentType.MediaType, responseData);
                                break;
                            default:
                                responseGeneric.ResponseError = DeserializeResponse<TResponseErrorExpected>(httpResponse.Content.Headers.ContentType.MediaType, responseData);
                                break;
                        }
                    }
                }
            }

            return responseGeneric;
        }

        /// <summary>
        /// Method that execute asynchronous http DELETE request
        /// </summary>
        /// <typeparam name="TResponseSuccessExpected">Type of Response if call return status equals to 200</typeparam>
        /// <typeparam name="TResponseErrorExpected">Type of Response if call not return status equals to 200</typeparam>
        /// <param name="baseAddress">Base address of request</param>
        /// <param name="requestUri">The request uri of the call</param>
        /// <param name="paramsData">The object that contains the params of the call</param>
        /// <param name="httpRequestOptions">The option of the request</param>
        /// <returns>Task of ResponseGeneric Object with the response of the request</returns>
        public static async Task<ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>> ExecuteDeleteCallAsync<TResponseSuccessExpected, TResponseErrorExpected>(Uri baseAddress,
            Uri requestUri, IParams paramsData = null, HttpRequestSettings httpRequestOptions = null)
            where TResponseSuccessExpected : IResponse where TResponseErrorExpected : IResponse
        {
            // Execute Controls
            if (paramsData != null) paramsData.ValidateData();
            if (httpRequestOptions == null) httpRequestOptions = new HttpRequestSettings();
            requestUri = ResolveUriWithParams(requestUri, paramsData);

            ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected> responseGeneric = new ResponseGeneric<TResponseSuccessExpected, TResponseErrorExpected>();

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {

                // Adding Headers if they are setted
                if (httpRequestOptions.Headers != null)
                {
                    foreach (KeyValuePair<string, string> header in httpRequestOptions.Headers)
                    {
                        httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
                    }
                }

                // Execute DELETE Async Method
                using (var httpResponse = await httpClient.DeleteAsync(requestUri))
                {
                    string responseData = await httpResponse.Content.ReadAsStringAsync();
                    responseGeneric.RequestStatus = httpResponse.StatusCode;

                    switch (httpResponse.StatusCode)
                    {
                        case HttpStatusCode.OK:
                            responseGeneric.ResponseSuccess = DeserializeResponse<TResponseSuccessExpected>(httpResponse.Content.Headers.ContentType.MediaType, responseData);
                            break;
                        default:
                            responseGeneric.ResponseError = DeserializeResponse<TResponseErrorExpected>(httpResponse.Content.Headers.ContentType.MediaType, responseData);
                            break;
                    }
                }
            }

            return responseGeneric;
        }

        #endregion


        #region Internal Methods


        /// <summary>
        /// This method deserialize the response of the request into a object based on the content type returned.
        /// </summary>
        /// <typeparam name="TResponseExpected"></typeparam>
        /// <param name="contentType"></param>
        /// <param name="responseData"></param>
        /// <returns></returns>
        internal static TResponseExpected DeserializeResponse<TResponseExpected>(string contentType, string responseData)
        {
            switch (contentType)
            {
                case "application/json":
                    return JsonConvert.DeserializeObject<TResponseExpected>(responseData);

                default: throw new Exception("ContentType '" + contentType + "' not supported for response data yet!");
            }
        }

        /// <summary>
        /// This method serialize the request data of the request
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        internal static StringContent SerializeRequest(string contentType, IRequest requestData)
        {
            string contentRequestData;

            switch (contentType)
            {
                case "application/json":
                    contentRequestData = requestData != null ? JsonConvert.SerializeObject(requestData) : "{}";
                    break;

                default: throw new Exception("ContentType '" + contentType + "' not supported for request data yet!");
            }

            return new StringContent(contentRequestData, Encoding.Default, contentType);
        }

        /// <summary>
        /// Resolve the uri with the params data
        /// </summary>
        /// <param name="requestUri">Request Uri of the request</param>
        /// <param name="paramsData">Object with the params</param>
        /// <returns>Correct Uri with replacement of params</returns>
        internal static Uri ResolveUriWithParams(Uri requestUri, IParams paramsData)
        {
            Uri retUri = requestUri;

            if (paramsData != null)
            {
                string getParams = string.Empty;
                string requestUriToCheck = requestUri.ToString();

                // Get all params between '{' and '}' in uri.
                List<Cache.PropertyInfoSupport> propertiesInUri = new List<Cache.PropertyInfoSupport>();
                List<Cache.PropertyInfoSupport> propertiesObjParams = paramsData.GetType().GetPropertiesCached();
                MatchCollection matches = Regex.Matches(requestUriToCheck, @"{.*}");    // I search if string contains parts with this structure '{SOME_TEXT}' and I extract all parts (with '{' at the start and '}' at the end)
                
                // Check if I found some part of string to substitute
                if (matches.Count != 0)
                {
                    // Cycle Matches 
                    foreach (Match match in matches)
                    {
                        foreach (Capture capture in match.Captures)
                        {
                            // I Search if there is a property that have the name equals to the value finded.
                            // NOTE: The name will be extract by 'GetTrueNameOfParamProperty' because can I have a params object with ParamProperty Attribute
                            Cache.PropertyInfoSupport propertyInfoSupportFinded = propertiesObjParams.Find(f => "{" + GetTrueNameOfParamProperty(f) + "}" == capture.Value);    // The capture contain '{' and '}'
                            if (propertyInfoSupportFinded != null)
                            {
                                string propertyInfoSupportFindedValueSerialized = GetValueSerializedOfParamProperty(propertyInfoSupportFinded, paramsData);
                                if (!string.IsNullOrEmpty(propertyInfoSupportFindedValueSerialized))
                                {
                                    requestUriToCheck = requestUriToCheck.Replace(capture.Value, propertyInfoSupportFindedValueSerialized);
                                    propertiesInUri.Add(propertyInfoSupportFinded);
                                }
                                else
                                {
                                    throw new Exception("The value of property '" + propertyInfoSupportFinded.PropertyInfo.Name + "', that it's a inner param of the request Uri, can't be null or empty!");
                                }
                            }
                        }
                    }
                }

                // Remove from the list 'propertiesObjParams' the list of 'propertiesInUri' I can't reuse it on params data
                propertiesObjParams = propertiesObjParams.Except(propertiesInUri).ToList();

                // Cycle the object and create get params string
                Dictionary<string, string> KeyValueGetParams = new Dictionary<string, string>();

                propertiesObjParams.ForEach(propertyInfoSupport =>
                {
                    if (propertyInfoSupport != null)
                    {
                        string propertyInfoSupportName = GetTrueNameOfParamProperty(propertyInfoSupport);
                        string propertyInfoSupportValue = GetValueSerializedOfParamProperty(propertyInfoSupport, paramsData, propertyInfoSupportName);

                        if (!string.IsNullOrEmpty(propertyInfoSupportValue))
                        {
                            getParams += (!string.IsNullOrEmpty(getParams) ? "&" : string.Empty) + propertyInfoSupportName + "=" + propertyInfoSupportValue;
                        }
                    }
                });

                // Create the correct url with get params
                if (!string.IsNullOrEmpty(getParams))
                {
                    requestUriToCheck = requestUriToCheck + (requestUriToCheck.Contains('?') ? "&" : "?") + getParams;
                }

                retUri = new Uri(requestUriToCheck, UriKind.Relative);
            }

            return retUri;
        }

        /// <summary>
        /// Get the True name of the param property
        /// </summary>
        /// <param name="propertyInfoSupport">PropertyInfoSupport object</param>
        /// <returns>The string that contains the correcet name of the param property</returns>
        internal static string GetTrueNameOfParamProperty(Cache.PropertyInfoSupport propertyInfoSupport)
        {
            if (propertyInfoSupport.ParamPropertyAssociated != null) return propertyInfoSupport.ParamPropertyAssociated.NameParam;
            return propertyInfoSupport.PropertyInfo.Name;
        }

        /// <summary>
        /// Get the serialized value of the property of the param object
        /// </summary>
        /// <param name="propertyInfoSupport">Property Info Support object</param>
        /// <param name="param">The param object</param>
        /// <param name="nameGetParam">The name of the get param. Used for params not in request uri</param>
        /// <returns></returns>
        internal static string GetValueSerializedOfParamProperty(Cache.PropertyInfoSupport propertyInfoSupport, IParams param, string nameGetParam = null)
        {
            string valueSerialized = string.Empty;

            if (propertyInfoSupport != null)
            {
                // I Get Value and Type
                Type propertyInfoSupportType = propertyInfoSupport.PropertyInfo.PropertyType;
                object propertyInfoSupportFindedValue = propertyInfoSupport.PropertyInfo.GetValue(param);

                if (propertyInfoSupportFindedValue != null)
                {
                    // Check if the param property has a converter
                    if (propertyInfoSupport.ParamConverterAssociated != null)
                    {
                        ParamConverter paramConverter = (ParamConverter)Activator.CreateInstance(propertyInfoSupport.ParamConverterAssociated.ConverterType);

                        if (paramConverter.CanConvert(propertyInfoSupportType))
                        {
                            valueSerialized = paramConverter.SerializeParamValue(propertyInfoSupportFindedValue);
                        }
                        else
                        {
                            throw new Exception("The Converter '" + paramConverter.GetType().Name + "' can't be able to convert '" + propertyInfoSupportType.Name + "'.");
                        }
                    }
                    else
                    {
                        valueSerialized = propertyInfoSupportFindedValue.ToString();
                    }
                }
            }

            if (!string.IsNullOrEmpty(nameGetParam))
            {
                valueSerialized = valueSerialized.Replace("{NAME_PARAM}", nameGetParam);
            }

            return valueSerialized;
        }

        #endregion

    }

}
