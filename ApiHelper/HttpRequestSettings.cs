﻿using System;
using System.Collections.Generic;

namespace PaironsTech.ApiHelper
{

    /// <summary>
    /// Useful object for manage settings of the request
    /// </summary>
    public class HttpRequestSettings
    {

        #region Private Attributes

        /// <summary>
        /// Private Attributes that contain the content type of the request
        /// </summary>
        private string _contentTypeRequest { get; set; }

        #endregion


        #region Public Attributes

        /// <summary>
        /// Dictionary of Headers to add at the request
        /// </summary>
        public Dictionary<string, string> Headers { get; set; }

        /// <summary>
        /// Content Type of the Request
        /// </summary>
        public string ContentTypeRequest
        {
            get { return _contentTypeRequest; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _contentTypeRequest = value;
                }
                else
                {
                    throw new Exception("It's not possible set empty or null content type!");
                }
            }
        }

        #endregion





        /// <summary>
        /// Constructor of HttpRequestSettings Object
        /// </summary>
        public HttpRequestSettings()
        {
            Headers = new Dictionary<string, string>();
            ContentTypeRequest = "application/json";
        }

    }

}
